const express = require('express');

const router = express.Router();

const {transactionsControllers}  = require('../controller/transactionsControllers')

/* router.get('/', (req,res) => res.send("Hello transaction.js"));
 */
router
.route('/')
.get(transactionsControllers.get)
.post(transactionsControllers.post)

router
.route('/:id')
.delete(transactionsControllers.delete) 

module.exports = router;