import { Transaction } from "../entity/entity.js";
import { connection } from "./connection.js";
import { transactionsControllers } from "../controller/transactionsControllers.js";




export async function getTransactions() {


    const [rows] = await connection.execute('SELECT * FROM transactions');
    const transactions = [];
    for (const row of rows) {
        let instance = new Transaction(row.id, row.text, row.amount, row.createdDate);
        transactions.push(instance);

    }
    return transactions;
}


export async function addTransaction(transaction) {
    const [rows] = await connection.query('INSERT INTO transactions (text,amount,createdDate) VALUES (?,?,?)', [transaction.text, transaction.amount, transaction.createdDate]);
    transaction.id = rows.insertId;

}

export async function deleteTransaction(id) {
    await connection.query('DELETE FROM transactions WHERE id=?', [id]);
}



export async function getTransaction(id) {
    const [rows] = await connection.execute('SELECT * FROM transactions WHERE id=?', [id]);
    if (rows.length === 1) {
        return new Transaction(rows[0].id, rows[0].text, rows[0].amount, rows[0].createdDate);
    }

    return null;
}



