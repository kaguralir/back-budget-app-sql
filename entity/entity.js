export class Transaction {
    id;
    text;
    amount;
    date;
    /**
     * 
     * @param {number} id 
     * @param {string} text 
     * @param {number} amount 
     * @param {number} date 
     */
    constructor(id,text, amount, date){
        this.id = id;
        this.text = text;
        this.amount = amount;
        this.date = date;

    }
}