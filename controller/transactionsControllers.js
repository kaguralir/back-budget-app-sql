import { Router } from "express";
import { getTransactions, addTransaction, deleteTransaction, getTransaction} from "../repository/Transaction.js";


export const transactionsControllers = Router();


// @desc    Get all transactions
// @route   GET /api/v1/transactions
// @access  Public
transactionsControllers.get('/', async (req, res) => {
    try {
        const transactions = await getTransactions();

        return res.status(200).json({
            success: true,
            count: transactions.length,
            data: transactions
        });
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }
});


// @desc    Add transaction
// @route   POST /api/v1/transactions
// @access  Public


transactionsControllers.post('/', async (req, res) => {
    try {
        const { text, amount } = req.body;

        const transaction = await addTransaction (req.body);

        return res.status(201).json({
            success: true,
            data:req.body
        });
    } catch (err) {
        console.log(err);
        if (err.name === 'ValidationError') {
            const messages = Object.values(err.errors).map(val => val.message);

            return res.status(400).json({
                success: false,
                error: messages
            });
        } else {
            return res.status(500).json({
                success: false,
                error: 'Server Error'
            });
        }
    }
})



// @desc    Delete transaction
// @route   DELETE /api/v1/transactions/:id
// @access  Public
transactionsControllers.delete('/:id', async (req, res) => {

    await deleteTransaction(req.params.id);
    res.end();
    /*     try {
        const transaction = await deleteTransaction.findById(req.params.id);

        if (!transaction) {
            return res.status(404).json({
                success: false,
                error: 'No transaction found'
            });
        }

        await transaction.remove();

        return res.status(200).json({
            success: true,
            data: {}
        });

    } catch (err) {
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    } */
})


// @desc    Get transaction
// @route   Get /api/v1/transactions/:id
// @access  Public
transactionsControllers.get('/:id', async (req, resp) => {
    let transaction = await getTransaction(req.params.id);
    if(!transaction) {
        resp.status(404).json({error: 'Not Found'});
        return;
    }
    resp.json(transaction);
})