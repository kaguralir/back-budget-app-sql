import 'dotenv-flow/config.js';

import express from 'express';


import cors from 'cors';
import { transactionsControllers } from './controller/transactionsControllers.js';



export const server = express();

server.use(express.json());
server.use(cors());

server.use('/api/v1/transactions', transactionsControllers)

const port = process.env.PORT || 8000;


server.listen(port, () => {
    console.log('listening on port ' + 8000);
});

process.on('SIGINT', () => {
    console.log("exiting…");
    process.exit(0);
});

process.on('exit', () => {
    console.log("exiting…");
    process.exit(0);
});
process.on('uncaughtException', err => {
    console.log(err, 'Uncaught Exception thrown');
    process.exit(0);
});

